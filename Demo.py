#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import nltk
import keras
import joblib

os.environ['TF_KERAS'] = '1'
import pandas as pd
import numpy as np

from math import ceil
from gensim.models import Word2Vec
from keras.models import load_model
from sklearn.pipeline import make_pipeline
from keras.preprocessing.text import Tokenizer
from keras_bert import load_trained_model_from_checkpoint
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer


# In[2]:


result = {}
num_words=1000
max_sentence_length = 36
MODEL_DIR = 'Saved model/'


# In[3]:


LR_countvector = 'LR Countvectorizer.sav'
LR_TFIDF = 'LR TF-IDF.sav'
LR_countvector_bigram = 'LR Countvectorizer Bi-gram.sav'
SVM = 'Support Vector Machine.sav'

LSTM_1_layer = '1-Layer LSTM.h5'
LSTM_1_layer_weight = '1-Layer LSTM_weights.h5'
BILSTM_2_layer = '2-Layer Bi-LSTM.h5'
BILSTM_2_layer_weight = '2-Layer Bi-LSTM_weights.h5'
BILSTM_2_layer_Word2Vec = '2-Layer Bi-LSTM Word2Vec.h5'
BILSTM_2_layer_Word2Vec_weight = '2-Layer Bi-LSTM Word2Vec_weights.h5'

BERT = 'BERT_training'
config_path = os.path.join(BERT, 'bert_config.json')
trained_checkpoint_path = os.path.join(BERT, 'cp.ckpt')


# In[4]:


train = pd.read_csv('data/train_data_restaurant.tsv', names = ['text' , 'target'], delimiter='\t', header=None)
X_train = train['text'].tolist()


# In[5]:


x_pred = input("Masukkan teks: ")


# In[6]:


def machine_learning(model):
    model = joblib.load(os.path.join(MODEL_DIR, model))
    result = model.predict([x_pred])[0]
    return result


# In[7]:


def LSTM(model):
    tok = Tokenizer(num_words=num_words)
    data = tok.texts_to_matrix([x_pred])
    data = np.reshape(data, (data.shape[0], 1, data.shape[1]))

    model = load_model(os.path.join(MODEL_DIR, model))
    result = model.predict_classes(data)[0][0]
    return result


# In[8]:


def padding(post, max_sentence_length=max_sentence_length):
    # Pad short posts with alternating min/max
    if len(post) < max_sentence_length:

            # Method 1
        pointwise_min = np.minimum.reduce(post)
        pointwise_max = np.maximum.reduce(post)
        padding = [pointwise_max, pointwise_min]

            # Method 2
            #pointwise_avg = np.mean(post)
            #padding = [pointwise_avg]

        post += padding * ceil((max_sentence_length - len(post) / 2.0))

        # Shorten long posts or those odd number length posts we padded to 51
    if len(post) > max_sentence_length:
        post = post[:max_sentence_length]
        
    return post      


# In[9]:


def LSTM_Word2Vec(model):
    embedding = Word2Vec.load("Word2Vec/id.bin")
    x_pred_tok = nltk.word_tokenize(x_pred.lower())
    x_pred_vec = [embedding.wv[w] for w in x_pred_tok if w in embedding.wv.vocab]
    x_pred_pad = padding(x_pred_vec)
    x_test = np.array(x_pred_pad)
    x_test = np.reshape(x_test, (1, x_test.shape[0], x_test.shape[1]))
    
    model = load_model(os.path.join(MODEL_DIR, model))
    result = model.predict_classes(x_test)[0][0]
    return result


# In[10]:


def get_feature (tokenizer, data_text, label, max_len):
    indices, sentiments = [], []
    for i, text in enumerate(data_text):
        ids, segments = tokenizer.encode(text[0], max_len=max_len)
        indices.append(ids)
        sentiments.append(label[i])
    items = list(zip(indices, sentiments))
    np.random.shuffle(items)
    indices, sentiments = zip(*items)
    indices = np.array(indices)
    mod = indices.shape[0] % 32
    if mod > 0:
        indices, sentiments = indices[:-mod], sentiments[:-mod]
    return [indices, np.zeros_like(indices)], np.array(sentiments)


# In[11]:


def get_BERT_model():
    import tensorflow as tf
    import tensorflow.keras
    
    from keras import optimizers
    from tensorflow.python import keras
    from keras_bert import load_trained_model_from_checkpoint
    
    pretrained_path = 'multi_cased_L-12_H-768_A-12/'
    config_path = os.path.join(pretrained_path, 'bert_config.json')
    checkpoint_path = os.path.join(pretrained_path, 'bert_model.ckpt')
    
    bert_max_length = 128
    
    model = load_trained_model_from_checkpoint(config_path, 
                                           checkpoint_path, 
                                           training=True, 
                                           trainable=True, 
                                           seq_len=bert_max_length)
    
    inputs = model.inputs[:2]
    dense = model.get_layer('NSP-Dense').output
    drop = keras.layers.Dropout(0.3)(dense)
    dense = keras.layers.Dense(64, activation='sigmoid')(drop)
    outputs = keras.layers.Dense(units=1, activation='sigmoid')(dense)
    model = keras.models.Model(inputs, outputs)
    
    model.load_weights(trained_checkpoint_path)
    
    tok = Tokenizer(num_words=bert_max_length)
    tok.fit_on_texts(X_train)
    data = tok.texts_to_matrix([x_pred])
    
    data = [data, (np.zeros((1, 128)))]
    
    result = model.predict(data)[0][0]
    result = 0 if result <= 0.5 else 1
    
    return result


# In[12]:


result['LR Countvectorizer'] = machine_learning(LR_countvector)
result['LR TF-IDF'] = machine_learning(LR_TFIDF)
result['LR Countvectorizer Bi-gram'] = machine_learning(LR_countvector_bigram)
result['Support Vector Machine'] = machine_learning(SVM)


# In[13]:


result['1-Layer LSTM'] = LSTM(LSTM_1_layer)
result['2-Layer Bi-LSTM'] = LSTM(BILSTM_2_layer)


# In[14]:


result['2-Layer Bi-LSTM Word2Vec'] = LSTM_Word2Vec(BILSTM_2_layer_Word2Vec)


# In[15]:


result['BERT'] = get_BERT_model()


# In[16]:


for model, sentiment in result.items():
    result[model] = "Positive" if sentiment == 1 else "Negative"


# In[17]:


for model, sentiment in result.items():
    print(model +": "+sentiment)


# In[ ]:




